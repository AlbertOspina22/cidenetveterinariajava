# CidenetVeterinariaJava

## Descripción

En este proyecto se crea una API para una veterinaria. Está construida en springboot, recibe comunicación mediente el protocolo SOAP y se conecta a una base de datos donde se almacenan los datos de los pacientes. La API permite realizar las operaciones para crear y consultar pacientes.


## Tecnologias utilizadas

- Java 1.8
- Springboot 2.7.5
- MySQL


## Infromación importante

### Conexion a BD

En el archivo application.properties de la carpeta resources están los datos de conexión a la BD.

### Estructura del tabla y contenido

Dentro de la carpeta application.properties se encuentra el archivo DDL de la tabla vetpacientes, tabla donde se almacenan los datos de los pacientes, y además se añadió un archivo con datos de ejemplo en caso de ser necesario.

### Datos de conexion con la API

Al ejecutar la API correctamente esta desplegará el WSDL en la dirección: http://localhost:8090/cidenet/veterinaria.wsdl
Cabe destacar que al ejecutarse la API recibirá las peticiones en la dirección: http://localhost:8090/cidenet


