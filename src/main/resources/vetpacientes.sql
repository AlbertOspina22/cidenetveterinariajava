CREATE TABLE `vetpacientes` (
	`codigo_chip` VARCHAR(10) NOT NULL DEFAULT '' COLLATE 'utf8mb4_0900_ai_ci',
	`nombre` VARCHAR(25) NOT NULL COLLATE 'utf8mb4_0900_ai_ci',
	`especie` VARCHAR(25) NOT NULL COLLATE 'utf8mb4_0900_ai_ci',
	`subespecie` VARCHAR(25) NULL DEFAULT NULL COLLATE 'utf8mb4_0900_ai_ci',
	`edad` TINYINT(3) NOT NULL DEFAULT '0',
	`observacion` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_0900_ai_ci',
	PRIMARY KEY (`codigo_chip`) USING BTREE
)
COLLATE='utf8mb4_0900_ai_ci'
ENGINE=InnoDB
;