INSERT INTO `vetpacientes` (`codigo_chip`, `nombre`, `especie`, `subespecie`, `edad`, `observacion`) VALUES ('AAA1234567', 'RALF', 'HAMSTER', 'RUSO', 1, 'SANO');
INSERT INTO `vetpacientes` (`codigo_chip`, `nombre`, `especie`, `subespecie`, `edad`, `observacion`) VALUES ('BBB1234567', 'JUAN', 'PERRO', 'SAN BERNARDO', 4, 'SANO');
INSERT INTO `vetpacientes` (`codigo_chip`, `nombre`, `especie`, `subespecie`, `edad`, `observacion`) VALUES ('CCC1234567', 'LALO', 'GATO', 'PERSA', 2, 'VOMITOS');
INSERT INTO `vetpacientes` (`codigo_chip`, `nombre`, `especie`, `subespecie`, `edad`, `observacion`) VALUES ('DDD1234567', 'LOLA', 'GATO', 'COMUN', 2, 'SANO');
INSERT INTO `vetpacientes` (`codigo_chip`, `nombre`, `especie`, `subespecie`, `edad`, `observacion`) VALUES ('EEE1234567', 'RUFUS', 'LORO', 'COMUN', 3, 'ALA ROTA');
