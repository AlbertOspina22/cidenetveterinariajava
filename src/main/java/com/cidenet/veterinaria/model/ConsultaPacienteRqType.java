//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2023.05.12 a las 10:50:09 AM COT 
//


package com.cidenet.veterinaria.model;

import javax.xml.bind.annotation.*;


/**
 * <p>Clase Java para ConsultaPacienteRq_Type complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ConsultaPacienteRq_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoChip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlRootElement(name = "ConsultaPacienteRq")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultaPacienteRq_Type", propOrder = {
    "codigoChip"
})
public class ConsultaPacienteRqType {

    @XmlElement(name = "CodigoChip", required = true)
    protected String codigoChip;

    /**
     * Obtiene el valor de la propiedad codigoChip.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoChip() {
        return codigoChip;
    }

    /**
     * Define el valor de la propiedad codigoChip.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoChip(String value) {
        this.codigoChip = value;
    }

}
