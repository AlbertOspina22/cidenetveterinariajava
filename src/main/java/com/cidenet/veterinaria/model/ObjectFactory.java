//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2023.05.12 a las 10:50:09 AM COT 
//


package com.cidenet.veterinaria.model;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cidenet.osb.veterinaria package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CrearPacienteRs_QNAME = new QName("http://veterinaria.osb.cidenet", "CrearPacienteRs");
    private final static QName _CrearPacienteRq_QNAME = new QName("http://veterinaria.osb.cidenet", "CrearPacienteRq");
    private final static QName _ConsultaPacienteRq_QNAME = new QName("http://veterinaria.osb.cidenet", "ConsultaPacienteRq");
    private final static QName _ConsultaPacienteRs_QNAME = new QName("http://veterinaria.osb.cidenet", "ConsultaPacienteRs");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cidenet.osb.veterinaria
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CrearPacienteRsType }
     * 
     */
    public CrearPacienteRsType createCrearPacienteRsType() {
        return new CrearPacienteRsType();
    }

    /**
     * Create an instance of {@link ConsultaPacienteRsType }
     * 
     */
    public ConsultaPacienteRsType createConsultaPacienteRsType() {
        return new ConsultaPacienteRsType();
    }

    /**
     * Create an instance of {@link ConsultaPacienteRqType }
     * 
     */
    public ConsultaPacienteRqType createConsultaPacienteRqType() {
        return new ConsultaPacienteRqType();
    }

    /**
     * Create an instance of {@link PacienteType }
     * 
     */
    public PacienteType createPacienteType() {
        return new PacienteType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CrearPacienteRsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://veterinaria.osb.cidenet", name = "CrearPacienteRs")
    public JAXBElement<CrearPacienteRsType> createCrearPacienteRs(CrearPacienteRsType value) {
        return new JAXBElement<CrearPacienteRsType>(_CrearPacienteRs_QNAME, CrearPacienteRsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PacienteType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://veterinaria.osb.cidenet", name = "CrearPacienteRq")
    public JAXBElement<PacienteType> createCrearPacienteRq(PacienteType value) {
        return new JAXBElement<PacienteType>(_CrearPacienteRq_QNAME, PacienteType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaPacienteRqType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://veterinaria.osb.cidenet", name = "ConsultaPacienteRq")
    public JAXBElement<ConsultaPacienteRqType> createConsultaPacienteRq(ConsultaPacienteRqType value) {
        return new JAXBElement<ConsultaPacienteRqType>(_ConsultaPacienteRq_QNAME, ConsultaPacienteRqType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultaPacienteRsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://veterinaria.osb.cidenet", name = "ConsultaPacienteRs")
    public JAXBElement<ConsultaPacienteRsType> createConsultaPacienteRs(ConsultaPacienteRsType value) {
        return new JAXBElement<ConsultaPacienteRsType>(_ConsultaPacienteRs_QNAME, ConsultaPacienteRsType.class, null, value);
    }

}
