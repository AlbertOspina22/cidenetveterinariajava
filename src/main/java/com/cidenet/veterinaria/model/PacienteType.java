//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2023.05.12 a las 10:50:09 AM COT 
//


package com.cidenet.veterinaria.model;

import javax.xml.bind.annotation.*;


/**
 * <p>Clase Java para Paciente_Type complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Paciente_Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoChip" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Nombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Especie" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SubEspecie" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Edad" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Observacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlRootElement(name = "CrearPacienteRq")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Paciente_Type", propOrder = {
    "codigoChip",
    "nombre",
    "especie",
    "subEspecie",
    "edad",
    "observacion"
})
public class PacienteType {

    @XmlElement(name = "CodigoChip", required = true)
    protected String codigoChip;
    @XmlElement(name = "Nombre", required = true)
    protected String nombre;
    @XmlElement(name = "Especie", required = true)
    protected String especie;
    @XmlElement(name = "SubEspecie", required = true)
    protected String subEspecie;
    @XmlElement(name = "Edad")
    protected int edad;
    @XmlElement(name = "Observacion", required = true)
    protected String observacion;

    /**
     * Obtiene el valor de la propiedad codigoChip.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoChip() {
        return codigoChip;
    }

    /**
     * Define el valor de la propiedad codigoChip.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoChip(String value) {
        this.codigoChip = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad especie.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEspecie() {
        return especie;
    }

    /**
     * Define el valor de la propiedad especie.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEspecie(String value) {
        this.especie = value;
    }

    /**
     * Obtiene el valor de la propiedad subEspecie.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubEspecie() {
        return subEspecie;
    }

    /**
     * Define el valor de la propiedad subEspecie.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubEspecie(String value) {
        this.subEspecie = value;
    }

    /**
     * Obtiene el valor de la propiedad edad.
     * 
     */
    public int getEdad() {
        return edad;
    }

    /**
     * Define el valor de la propiedad edad.
     * 
     */
    public void setEdad(int value) {
        this.edad = value;
    }

    /**
     * Obtiene el valor de la propiedad observacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacion() {
        return observacion;
    }

    /**
     * Define el valor de la propiedad observacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacion(String value) {
        this.observacion = value;
    }

}
