package com.cidenet.veterinaria.model.entity;

import lombok.Data;
import org.springframework.data.domain.Persistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vetpacientes")
@Data
public class VetPaciente implements Persistable<String> {

    @Id
    @Column(name = "codigo_chip")
    private String codigoChip;
    private String nombre;
    private String especie;
    private String subespecie;
    private Integer edad;
    private String observacion;

    @Override
    public String getId() {
        return null;
    }

    @Override
    public boolean isNew() {
        return true;
    }
}
