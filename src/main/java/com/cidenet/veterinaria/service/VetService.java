package com.cidenet.veterinaria.service;

import com.cidenet.veterinaria.model.ConsultaPacienteRsType;
import com.cidenet.veterinaria.model.CrearPacienteRsType;
import com.cidenet.veterinaria.model.PacienteType;


public interface VetService {

    public ConsultaPacienteRsType findPaciente(String id);

    public CrearPacienteRsType savePaciente(PacienteType paciente);


}
