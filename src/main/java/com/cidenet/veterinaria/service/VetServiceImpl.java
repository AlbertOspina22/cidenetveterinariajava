package com.cidenet.veterinaria.service;

import com.cidenet.veterinaria.model.ConsultaPacienteRsType;
import com.cidenet.veterinaria.model.CrearPacienteRsType;
import com.cidenet.veterinaria.model.PacienteType;
import com.cidenet.veterinaria.model.entity.VetPaciente;
import com.cidenet.veterinaria.repository.VetRepo;
import com.cidenet.veterinaria.utils.Converter;
import net.bytebuddy.dynamic.DynamicType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Locale;
import java.util.Optional;


@Service
public class VetServiceImpl implements VetService {

    private VetRepo repo;
    private Converter converter;

    @Autowired
    public VetServiceImpl(VetRepo repo, Converter converter) {
        this.repo = repo;
        this.converter = converter;
    }

    @Transactional(readOnly = true)
    @Override
    public ConsultaPacienteRsType findPaciente(String id) {
        ConsultaPacienteRsType consultaPacienteRsType = new ConsultaPacienteRsType();
        //Converter converter = new Converter();
        Optional<VetPaciente> pacienteResult = repo.findById(id);
        if (pacienteResult.isPresent()) {
            VetPaciente vetPaciente = pacienteResult.get();
            PacienteType pacienteRs = converter.toDto(vetPaciente);
            consultaPacienteRsType.setPaciente(pacienteRs);
            consultaPacienteRsType.setCodigo("0");
            consultaPacienteRsType.setMensaje("Encontrado");
        }
        else{
            consultaPacienteRsType.setCodigo("1");
            consultaPacienteRsType.setMensaje("No encontrado");
        }
        return consultaPacienteRsType;
    }

    @Transactional
    @Override
    public CrearPacienteRsType savePaciente(PacienteType paciente) {
        CrearPacienteRsType pacienteRs = new CrearPacienteRsType();
        //Converter converter = new Converter();
        VetPaciente vetPaciente = converter.toEntity(paciente);

        vetPaciente = repo.save(vetPaciente);

        pacienteRs.setCodigo(0);
        pacienteRs.setDescripcion("Guardado");
        return pacienteRs;
    }


}
