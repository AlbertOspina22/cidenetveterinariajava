package com.cidenet.veterinaria.utils;

import com.cidenet.veterinaria.model.PacienteType;
import com.cidenet.veterinaria.model.entity.VetPaciente;
import org.springframework.stereotype.Component;

@Component
public class Converter {

    public VetPaciente toEntity(PacienteType paciente){
        VetPaciente vetPaciente = new VetPaciente();
        vetPaciente.setCodigoChip(paciente.getCodigoChip());
        vetPaciente.setNombre(paciente.getNombre());
        vetPaciente.setEspecie(paciente.getEspecie());
        vetPaciente.setSubespecie(paciente.getSubEspecie());
        vetPaciente.setEdad(paciente.getEdad());
        vetPaciente.setObservacion(paciente.getObservacion());
        return vetPaciente;
    }


    public PacienteType toDto(VetPaciente vetPaciente){
        PacienteType pacienteType = new PacienteType();
        pacienteType.setCodigoChip(vetPaciente.getCodigoChip());
        pacienteType.setNombre(vetPaciente.getNombre());
        pacienteType.setEspecie(vetPaciente.getEspecie());
        pacienteType.setSubEspecie(vetPaciente.getSubespecie());
        pacienteType.setEdad(vetPaciente.getEdad());
        pacienteType.setObservacion(vetPaciente.getObservacion());
        return pacienteType;
    }
}
