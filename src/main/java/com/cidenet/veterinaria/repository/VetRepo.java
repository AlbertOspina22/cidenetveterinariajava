package com.cidenet.veterinaria.repository;

import com.cidenet.veterinaria.model.entity.VetPaciente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VetRepo extends CrudRepository<VetPaciente, String> {

}
