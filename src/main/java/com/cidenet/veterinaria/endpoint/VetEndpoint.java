package com.cidenet.veterinaria.endpoint;

import com.cidenet.veterinaria.model.ConsultaPacienteRqType;
import com.cidenet.veterinaria.model.ConsultaPacienteRsType;
import com.cidenet.veterinaria.model.CrearPacienteRsType;
import com.cidenet.veterinaria.model.PacienteType;
import com.cidenet.veterinaria.service.VetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class VetEndpoint {

    private static final String NAMESPACE = "http://veterinaria.osb.cidenet";

    private VetService vetService;

    @Autowired
    public VetEndpoint(VetService vetService){
        this.vetService = vetService;
    }


    @PayloadRoot(namespace = NAMESPACE, localPart = "ConsultaPacienteRq")
    @ResponsePayload
    public ConsultaPacienteRsType getPaciente(@RequestPayload ConsultaPacienteRqType pacienteRq){
        ConsultaPacienteRsType pacienteRs = new ConsultaPacienteRsType();

        try{
            pacienteRs = vetService.findPaciente(pacienteRq.getCodigoChip());
        }catch (Exception e){
            System.out.println(e);
            pacienteRs.setCodigo("1");
            pacienteRs.setMensaje("Error");
        }

        return pacienteRs;
    }

    @PayloadRoot(namespace = NAMESPACE, localPart = "CrearPacienteRq")
    @ResponsePayload
    public CrearPacienteRsType AddPaciente(@RequestPayload PacienteType pacienteRq){
        CrearPacienteRsType pacienteRs = new CrearPacienteRsType();

        try {
            pacienteRs = vetService.savePaciente(pacienteRq);
        }catch (Exception e)
        {
            System.out.println(e);
            pacienteRs.setCodigo(1);
            pacienteRs.setDescripcion("Error, no se guardó el paciente");
        }

        return pacienteRs;
    }
}
